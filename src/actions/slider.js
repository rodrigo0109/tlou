import { types } from '../types/types'

export const startOpenSlider = () => {
    return ( dispatch ) => {

        dispatch( open() )
        
    }
}

const open = () => ({ type: types.openSlider })

export const startCloseSlider = () => {
    return ( dispatch ) => {

        dispatch( close() )

    }
}

const close = () => ({ type: types.closeSlider })

/* export const startLogin = ( email, password ) => {
    return async( dispatch ) => {

        const resp = await fetchSinToken( 'auth', { email, password }, 'POST' )
        const body = await resp.json()

        if( body.ok ) {
            localStorage.setItem('token', body.token)
            localStorage.setItem('token-init-date', new Date().getTime() )

            dispatch( login({
                uid: body.uid,
                name: body.name
            }) )
        } else {
            Swal.fire('Error', body.msg, 'error')
        }

    }
} */

/* export const startRegister = ( email, password, name ) => {
    return async( dispatch ) => {

        const resp = await fetchSinToken( 'auth/new', { email, password, name }, 'POST' )
        const body = await resp.json()
        console.log(body)

        if( body.ok ) {
            localStorage.setItem('token', body.token)
            localStorage.setItem('token-init-date', new Date().getTime() )

            dispatch( login({
                uid: body.uid,
                name: body.name
            }) )
        } else {
            Swal.fire('Error', body.msg, 'error')
        }


    }
}

//para revalidar
export const startChecking = () => {
    return async( dispatch ) => {

        const resp = await fetchConToken( 'auth/renew' )
        const body = await resp.json()


        if( body.ok ) {
            localStorage.setItem('token', body.token)
            localStorage.setItem('token-init-date', new Date().getTime() )

            dispatch( login({
                uid: body.uid,
                name: body.name
            }) )
        } else {
            dispatch( checkingFinish() )
        }

    }
}

export const startLogout = () => {
    return ( dispatch ) => {

        localStorage.clear()
        dispatch( eventLogout() )
        dispatch( checkingLogout() )        
    
    }
}

const checkingLogout = () => ({ type: types.authLogout })

const checkingFinish = () => ({ type: types.authCheckingFinish })

const login = ( user ) => ({
    type: types.authLogin,
    payload: user
}) */