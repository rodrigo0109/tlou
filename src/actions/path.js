import { types } from '../types/types'


export const startTrueChange = () => {
    return ( dispatch ) => {

        dispatch( open() )
        
    }
}

const open = () => ({ type: types.trueChange })

export const startFalseChange = () => {
    return ( dispatch ) => {

        dispatch( close() )

    }
}

const close = () => ({ type: types.falseChange })



