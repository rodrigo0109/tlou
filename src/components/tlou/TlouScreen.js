import React from 'react'
import { About } from './About'
import { Footer } from './Footer'
import { Gallery } from './Gallery'
import { HeroSection } from './HeroSection'
import { PlayXp } from './PlayXp'
import { AboutGame } from './AboutGame'
import '../../styles.css'

export const TlouScreen = () => {
    return (
        <div className="screen">
            <HeroSection />
            <About />
            <Gallery />
            <PlayXp />
            <AboutGame />
            <Footer />
        </div>
    )
}
