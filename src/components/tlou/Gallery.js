import React from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { startOpenSlider } from '../../actions/slider'
import { SliderData } from '../../data/SliderData'
import ImageSlider from './ImageSlider'

export const Gallery = () => {

    const { checking } = useSelector( state => state.slider );
    const dispatch = useDispatch();

    const handleSlider = () => {
        dispatch( startOpenSlider() )
    }
 
    return (
        <div className="gallery-section">
            { !checking
                && 
                <div className="title-container animate__animated animate__fadeIn">
                    <h1>The Art of <br/> The Last Of US</h1>
                </div> 
            }

            {
                checking ?
                    <ImageSlider slides={ SliderData } />
                    :
                    <div className="image-container animate__animated animate__fadeIn">
                        <div className="col1">
                            <div className="c1-a" onClick={handleSlider}>
                            </div>
                            <div className="c1-b" onClick={handleSlider}>
                            </div>
                        </div>
                        <div className="col2" onClick={handleSlider}>
                        </div>
                    </div>
                }
        </div>
    )
}
