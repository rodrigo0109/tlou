import React, { useEffect, useState } from 'react'
import video from '../../videos/tlou_main.mp4'
import videoSplit from '../../videos/tlou_split.mp4'
import video2 from '../../videos/tlou_death_01.mp4'
import ReactPlayer from 'react-player'
import { btnTimes } from '../../data/btnTimes'
import { Link } from "react-scroll";
import { useSelector } from 'react-redux'

export const PlayXp = () => {

    const [action, setAction] = useState(null);
    const [startVideo, setStartVideo] = useState(false);
    const [counter, setCounter] = useState(0);
    const [clicks, setClicks] = useState(0);
    const [urlVideo, setUrlVideo] = useState(video);
    const [play, setPlay] = useState(true);
    const [repeat, setRepeat] = useState(false);
    const [split, setSplit] = useState(false);
    const [btnClass, setBtnClass] = useState();
    const [endTitle, setEndTitle] = useState();

    const { checking } = useSelector(state => state.slider);
    const first = 10
    const second = 15

    useEffect(() => {
        btnShow();
        processClick();
        handleEndTitle();
        handleExternalClose();
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [counter, checking])

    //Inicializo reproduccion
    const handleVideo = (e) => {
        e.preventDefault();
        setUrlVideo(video);
        setStartVideo(true);
        setPlay(false)
    }

    const handleVideoRepeat = (e) => {
        e.preventDefault();
        setRepeat(false);
        setStartVideo(true);
        setUrlVideo(video)
    }

    const { t1, t2, t3, t4, t5 } = btnTimes
    //Defino condicion para mostrar el boton de accion
    const btnShow = () => {
        if (counter >= t1[0]) {
            setBtnClass("btn-action");
            setAction(true);
        }
        if (counter >= t1[1]) {
            setAction(false);
        }
        if (counter >= t2[0]) {
            setBtnClass("btn-action2");
            setAction(true);
        }
        if (counter >= t2[1]) {
            setAction(false);
        }
        if (counter >= t3[0]) {
            setBtnClass("btn-action3");
            setAction(true);
        }
        if (counter >= t3[1]) {
            setAction(false);
        }
        if (urlVideo === videoSplit) {
            if (counter >= t4[0]) {
                setBtnClass("btn-action3");
                setAction(true);
            }
            if (counter >= t4[1]) {
                setAction(false);
            }
        }
    }
    //Incremento clicks
    const handleAction = (e) => {
        e.preventDefault();
        setClicks((prevState) => prevState + 1);
    }
    //Defino condicion para los clicks realizados
    const processClick = () => {
        if (counter >= t1[0] - 2 && counter <= t1[0]) setClicks(0);
        if (counter >= t1[1] && counter <= t1[1] + 1) {
            if (clicks < 5) {
                setUrlVideo(video2);
            } else console.log('No se hace nada PASO');
        }
        if (counter >= t2[0] - 2 && counter <= t2[0]) setClicks(0);
        if (counter >= t2[1] && counter <= t2[1] + 1) {
            if (clicks < first) {
                setUrlVideo(video2);
            } else console.log('No se hace nada PASO');
        }
        if (counter >= t3[0] - 2 && counter <= t3[0]) setClicks(0);
        if (counter >= t3[1] && counter <= t3[1] + 1) {
            if (clicks < second) {
                setUrlVideo(video2);
            } else console.log('No se hace nada PASO');
        }
        if (urlVideo === videoSplit) {
            if (counter >= t4[0] - 2 && counter <= t4[0]) setClicks(0);
            if (counter >= t4[1] && counter <= t4[1] + 1) {
                if (clicks < second) {
                    setUrlVideo(video2);
                } else console.log('No se hace nada PASO');
            }
        }
        if (counter >= t5[0] && counter <= t5[0] + 0.21) {
            setStartVideo(false);
            setSplit(true);
            console.log(counter, 'SPLIT');
        }
    }

    const handleClassEnded = () => {
        setRepeat(true);
        setStartVideo(false);
    }

    const handleVideoSplit = () => {
        setSplit(false);
        setUrlVideo(videoSplit);
        setStartVideo(true);
    }

    const handleVideoKill = () => {
        setSplit(false);
        setStartVideo(true);
    }

    const handleEndTitle = () => {
        if (urlVideo === videoSplit) {
            if (counter >= 44) {
                setEndTitle("You made it! Wanna try again?");
            }
            else setEndTitle("you died, try again");
        }
        else {
            if (counter >= 168) setEndTitle("You made it! Wanna try again?");
            else setEndTitle("you died, try again");
        }
    }

    const handleExit = () => {
        setUrlVideo(null);
        setPlay(true);
    }

    const handleExternalClose = () => {
        if (checking) {
            setUrlVideo(null);
            setPlay(true);
        }
    }

    return (
        <>
            <div className='xp-not animate__animated animate__fadeIn'>
                <h1>Take a look</h1>
                <h3>Only available for desktop version</h3>
            </div>
            <div className="xp">
                {
                    split && <div className='play-container animate__animated animate__fadeIn'>
                        <h1>Make a choice</h1>
                        <Link
                            className="btn-play"
                            to='section1'
                            spy={true}
                            smooth={true}
                            offset={70}
                            onClick={handleVideoSplit}
                        >
                            Run
                        </Link>
                        <Link
                            className="btn-play-kill"
                            to='section1'
                            spy={true}
                            smooth={true}
                            offset={70}
                            onClick={handleVideoKill}
                        >
                            Kill
                        </Link>
                    </div>
                }
                {
                    repeat && <div className='play-container animate__animated animate__fadeIn'>
                        <h1>{endTitle}</h1>
                        <p><i className="far fa-repeat"></i></p>
                        <Link
                            className="btn-play"
                            to='section1'
                            spy={true}
                            smooth={true}
                            offset={70}
                            onClick={handleVideoRepeat}
                        >
                            Go
                        </Link>
                    </div>
                }
                {
                    play && <div className='play-container animate__animated animate__fadeIn'>
                        <h1>Take a look</h1>
                        <p><i className="fas fa-headphones-alt"></i> <i className="fal fa-lightbulb-slash"></i></p>
                        <Link
                            className="btn-play"
                            to='section1'
                            spy={true}
                            smooth={true}
                            offset={70}
                            onClick={handleVideo}
                            volume={0}
                        >
                            Ready
                        </Link>
                    </div>
                }
                <div className="vid-container animate__animated animate__fadeIn">
                    {action &&
                        <>
                            <button className={btnClass} onClick={handleAction}><i class="far fa-solid fa-skull-crossbones"></i></button>
                            <div className='count_container'>
                                <p>Clicks</p>
                                {
                                    (clicks < 3 &&
                                    <span className='first'>{clicks}</span>)
                                    ||
                                    (clicks < 6 &&
                                    <span className='second'>{clicks}</span>)
                                    ||
                                    (clicks < 15 &&
                                    <span className='third'>{clicks}</span>)
                                    ||
                                    (clicks >= 15 &&
                                    <span className='success'>{clicks}</span>)
                                }
                            </div>
                        </>
                    }
                    <ReactPlayer
                        id='section1'
                        width='100%'
                        height='100%'
                        url={urlVideo}
                        //controls={true}
                        onEnded={() => handleClassEnded()}
                        playing={startVideo}
                        config={{
                            file: {
                                attributes: {
                                    onTimeUpdate: e => {
                                        let time = e.target.currentTime
                                        setCounter(time)
                                    },
                                    preload: 'true'
                                }
                            }
                        }
                        }
                    />
                    {!play && <button className="btn-vid-close" onClick={handleExit}><i className="fal fa-sign-out"></i></button>}
                </div>
            </div>
        </>
    )
}
