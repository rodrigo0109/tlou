import React from 'react'

export const AboutGame = () => {
    return (
        <div className='about-game'>
            <div className='game-img'></div>
            <div className='game-section'>
                <div className='game-text'>
                    <h1>Buy it now!</h1>
                    <p>Also available on PS5</p>
                </div>
                <div className='game-link'>
                    <a className='btn-buy' href="https://store.playstation.com/es-ni/product/UP9000-CUSA07820_00-THELASTOFUSPART2" rel="noreferrer"  target="_blank">Buy</a>
                </div>
            </div>
        </div>
    )
}
