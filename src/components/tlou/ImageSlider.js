import React, { useState } from 'react';
import { useDispatch } from 'react-redux';
import { startCloseSlider } from '../../actions/slider';
import { SliderData } from '../../data/SliderData';

const ImageSlider = ({ slides }) => {
  const [current, setCurrent] = useState(0);
  const dispatch = useDispatch();
  const length = slides.length;

  const handleClose = () => {

    dispatch( startCloseSlider() )

  }  

  const nextSlide = () => {
    setCurrent(current === length - 1 ? 0 : current + 1);
  };

  const prevSlide = () => {
    setCurrent(current === 0 ? length - 1 : current - 1);
  };

  if (!Array.isArray(slides) || slides.length <= 0) {
    return null;
  }

  return (
    <section className='slider animate__animated animate__fadeIn'>
      <button className='left-arrow' onClick={ prevSlide }><i className="fal fa-chevron-left"></i></button>
      <button className='right-arrow' onClick={ nextSlide }><i className="fal fa-chevron-right"></i></button>
      <button className='close' onClick={ handleClose }><i className="fal fa-times"></i></button>
      {SliderData.map((slide, index) => {
        return (
          <div
            className={index === current ? 'slide active' : 'slide'}
            key={index}
          >
            {index === current && (
              <img src={slide.image} alt='' className='image' />
            )}
          </div>
        );
      })}
    </section>
  );
};

export default ImageSlider;
