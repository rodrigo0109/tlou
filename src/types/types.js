export const types = {

    openSlider: '[slider] Open slider',
    closeSlider: '[slider] Close slider',

    trueChange: '[path] true change',
    falseChange: '[path] false change',
    noClickChange: '[path] no click',

}