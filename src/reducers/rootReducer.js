import { combineReducers } from "redux";
import { pathReducer } from "./pathReducer";
import { sliderReducer } from "./sliderReducer";



export const rootReducer = combineReducers({
    slider: sliderReducer,
    path: pathReducer,
})