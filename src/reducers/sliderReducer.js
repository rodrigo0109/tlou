import { types } from "../types/types";


const initialState = {
    checking: false
}

export const sliderReducer = ( state = initialState, action ) => {

    switch (action.type) {

        case types.openSlider:
            return {
                ...state,
                checking: true
            }
        
        case types.closeSlider:
            return {
                ...initialState
            }
    
        default:
            return state;
    }

}