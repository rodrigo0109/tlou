import { types } from "../types/types";


const initialState = {
    ok: false
}

export const pathReducer = (state = initialState, action) => {

    switch (action.type) {

        case types.trueChange:
            return {
                ...state,
                ok: true
            }

        case types.falseChange:
            return {
                ...initialState
            }

        case types.noClickChange:
            return {
                ...state
            }

        default:
            return state;
    }

}