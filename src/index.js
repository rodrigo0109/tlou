import React from 'react';
import ReactDOM from 'react-dom';
import { TlouApp } from './TlouApp';


ReactDOM.render(
    <TlouApp />,
    document.getElementById('root')
);

