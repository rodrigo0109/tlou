import React from 'react'
import { Provider } from 'react-redux'
import { TlouScreen } from './components/tlou/TlouScreen'
import { store } from './store/store'

export const TlouApp = () => {
  return (
    <Provider store={ store }>
      <TlouScreen />
    </Provider>
  )
}

